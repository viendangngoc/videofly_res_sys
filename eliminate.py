# -*- coding: utf-8 -*-
"""
Created on Wed Jul  4 00:58:48 2018

@author: ngocviendang
"""
def eliminate(string):
   mylist = string.split(',')
   seen = []
   for number in mylist:
       if number not in seen:
           seen.append(number)
   return ','.join(seen)
             

