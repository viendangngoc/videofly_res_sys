# -*- coding: utf-8 -*-
"""
Created on Wed Jul  4 08:13:47 2018

@author: ngocviendang
"""

#import libraries
import pandas as pd
#Read data
df = pd.read_json("video.json", lines=True, encoding="utf8")
#Remove missing values in 'tags' column
data = df[df['tags'].isnull() == False]
data1 = df[df['title'].isnull() == False]
tags = data['tags']
title = data1['title']
#convert tieng viet co dau sang tieng viet khong dau in tags column
tags_convert = tags.apply(lambda x: convert(x))  
#convert tieng viet co dau sang tieng viet khong dau in title column
title_convert = title.apply(lambda t: convert(t))
# eliminate identical in tags column
tags_eliminated = tags_convert.apply(lambda y: eliminate(y))
# eliminate abbrev words in tags colum
tags_remove_abbrev = tags_eliminated.apply(lambda z: remove_abbrev(z))



