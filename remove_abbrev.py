# -*- coding: utf-8 -*-
"""
Created on Wed Jul  4 11:08:47 2018

@author: ngocviendang
"""
def remove_abbrev(str):
    tagslist = str.lower().split(',')
    for number in tagslist:
        if find_abbrev(number) in tagslist:
            tagslist.remove(find_abbrev(number))
    return ','.join(tagslist)
